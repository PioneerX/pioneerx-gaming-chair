/*

PioneerX Gaming Chair controller
 - Logitech Driving Force Shifter (with SKRS)
 - Logitech G29 Pedal Base USB Adapter 
 - Internal Calibration
 - Combined Accelerator & Clutch axis for use as aircraft rudder
 - Combined Accelerator and Clutch axis for use as aircraft wheel brakes

NOTE: The rudder axis is implemented as as +512/-512 range with the difference between right and left pedal presses applied to the axis. 
Wheel brake is implemented as a 0/1024 range so that when both predals are pressed the common value between the two is applies to the axis, common values are igorned by the rudder axis.
Using wheel brakes reduces the angle of the steer on the rudder as it reduces the axis to axis difference btween throttle and clutch.

Original joystick only code by Armandoiglesias 2018
Rewritten by Simon Gutteridge (2020 & 2021) to support SKRS, G29 Pedals (with Truebrake upgrade), Calibration & aircraft use
Use Arduino Leonardo
Install Joystick Library - https://github.com/MHeironimus/ArduinoJoystickLibrary
Attribution-NonCommercial-NoDerivatives 4.0 International

Wiring
Arduino Pin   D9 Pin      Function
A0            1-4         G29 Sifter X-Axis
A2            1-8         G29 Sifter Y-Axis
D2            1-2         G29 Sifter Reverse Switch
D4            2-1         SKRS Range Switch
D7            2-3         SKRS Split Switch
A1            3-2         G29 Pedals Throttle
A3            3-3         G29 Pedals Brake
A10           3-4         G29 Pedals Clutch
5V            1-3,7       G29 Sifter 5V supply
5V            3-1         G29 Pedals 5V supply
GND           1-6         G29 Sifter Ground
GND           2-2,4       SKRS Ground
GND           3-6         G29 Pedals Ground
NOTE: All buttons/switches are grounded when pressed/on

*/

#include <Joystick.h>                 // Used to create a Windows HID of type joystick
#include <EEPROM.h>                   // Used to add permanent storage of calibration data
#include <AnalogPin.h>                // Used to smoothout the rough numbers coming from the noisy pots in the pedals

// H-shifter analog axis thresholds
#define HS_XAXIS_12        400
#define HS_XAXIS_56        600
#define HS_YAXIS_135       600
#define HS_YAXIS_246       400

// Digital inputs definitions
#define DI_REVERSE         2
#define DI_RANGE           4
#define DI_SPLIT           7

// Calibration Data
struct dataStore {
  int throttleMin;
  int throttleMax;
  int brakeMin;
  int brakeMax;
  int clutchMin;
  int clutchMax;
  int crcCheck;
};
dataStore calData;
int eeAddress = 0;                                                            // Location in EEPROM to put the calibration data.

// Analog vault smoothing & deadzones
AnalogPin INA(A1);
AnalogPin INB(A3);
AnalogPin INC(A10);
int throttleDeadzone = 130;
int brakeDeadzone = 100;
int clutchDeadzone = 110;
int wheelBrakeDeadzone = 100;

// Control State Flags
int joystickButtons[] = {DI_RANGE,DI_SPLIT};                                  // Pin assignments for SKRS buttons. Range, Split
int currentButtonState = 0;
int lastButtonState[] = {0,0};                                                // Last state of the buttons.  Range, Split
int currentGear = 0;
int lastGear = 0;
int plotData = 0;

// Create the Joystick with correct number of buttons and axis
Joystick_ Joystick(JOYSTICK_DEFAULT_REPORT_ID,JOYSTICK_TYPE_JOYSTICK,
  sizeof(joystickButtons) / sizeof(joystickButtons[0])+7, 0,                  // Button Count, Hat Switch Count
  true, true, true,                                                           // X, Y and Z Axis (Throttle, Brake, Clutch Pedals)
  false, false, false,                                                        // No Rx, Ry or Rz
  true, true,                                                                 // Rudder (using blended X & Z), Throttle (lowest of X & Z, intended for WheelBrakes)
  false, false, false);                                                       // No accelerator, brake, or steering (axis used instead)

void calibrate() {
  // Process calibration for the 3 axis  
  int currentStep = 0;
  int calPosition = 0;
  Serial.println("");
  Serial.println("*** Calibration begins ***");

  // Do minimum positions - all together
  calPosition = 0;
  currentStep = 0;
  Serial.println("Take foot of ALL pedals - Enter c to confirm");
  do
  {
    //wait for C to be send over serial
    if (Serial.available() > 0)
    {
      if (Serial.read() == 'c') 
      { 
        currentStep = 1;
      }
    }
  } while (currentStep == 0);
  calData.throttleMin = analogRead(1);
  calData.brakeMin = analogRead(3);
  calData.clutchMin = analogRead(10);
  Serial.print(" - Minimum position calibration data completed - Throttle:");
  Serial.print(calData.throttleMin);
  Serial.print(" Brake:");
  Serial.print(calData.brakeMin);
  Serial.print(" Clutch:");
  Serial.println(calData.clutchMin);
  Serial.println("");

  // Do Throttle MAX position
  calPosition = 0;
  currentStep = 0;
  Serial.println("Press THROTTLE pedal to max position 5 times and let go - Enter c to confirm");
  do
  {
    //wait for C to be send over serial
    if (Serial.available() > 0)
    {
      if (Serial.read() == 'c') 
      { 
        currentStep = 1;
      }
    }
    if (analogRead(1) > calPosition)
    {
      calPosition = analogRead(1);
    }
  } while (currentStep == 0);
  calData.throttleMax = calPosition;

  // Do Brake MAX position
  calPosition = 0;
  currentStep = 0;
  Serial.println("Press BRAKE pedal to max position 5 times and let go - Enter c to confirm");
  do
  {
    //wait for C to be send over serial
    if (Serial.available() > 0)
    {
      if (Serial.read() == 'c') 
      { 
        currentStep = 1;
      }
    }
    if (analogRead(3) > calPosition)
    {
      calPosition = analogRead(3);
    }
  } while (currentStep == 0);
  calData.brakeMax = calPosition;

  // Do Clutch MAX position
  calPosition = 0;
  currentStep = 0;
  Serial.println("Press CLUTCH pedal to max position 5 times and let go - Enter c to confirm");
  do
  {
    //wait for C to be send over serial
    if (Serial.available() > 0)
    {
      if (Serial.read() == 'c') 
      { 
        currentStep = 1;
      }
    }
    if (analogRead(10) > calPosition)
    {
      calPosition = analogRead(10);
    }
  } while (currentStep == 0);
  calData.clutchMax = calPosition;
  Serial.print(" - Maximum position calibration data completed - Throttle:");
  Serial.print(calData.throttleMax);
  Serial.print(" Brake:");
  Serial.print(calData.brakeMax);
  Serial.print(" Clutch:");
  Serial.println(calData.clutchMax);
  Serial.println("");

  calData.crcCheck = calData.throttleMin + calData.throttleMax + calData.brakeMin + calData.brakeMax + calData.clutchMin + calData.clutchMax;
  EEPROM.put(eeAddress, calData);                       // Add calibration data to permanent storage
  Serial.println("*** New calibrations stored ***");
  Serial.println("");
  delay(2000);
  menu();
}

void menu() {
  // Output the menu text
  Serial.println("Current Calibration data");
  Serial.print("  * Throttle:");
  Serial.print(calData.throttleMin);
  Serial.print("-");
  Serial.println(calData.throttleMax);
  Serial.print("  * Brake:");
  Serial.print(calData.brakeMin);
  Serial.print("-");
  Serial.println(calData.brakeMax);
  Serial.print("  * Clutch:");
  Serial.print(calData.clutchMin);
  Serial.print("-");
  Serial.println(calData.clutchMax);
  Serial.println("------------------------------------------------------");
  Serial.println("Select one of the following options:");
  Serial.println(" 1: Set new calibration");
  Serial.println(" 2: Erase existing calibrations");
  Serial.println(" 3: Enable/Disable data plotter");
  Serial.println("");
  Serial.println("");
}

void loadCals() {
  EEPROM.get(eeAddress, calData);               // Fetch the calibration data from the EEPROM (permanent storage)
  int dataCRC = calData.throttleMin + calData.throttleMax + calData.brakeMin + calData.brakeMax + calData.clutchMin + calData.clutchMax;
  if (dataCRC != calData.crcCheck)
  {
    //CRC does not match, no calibration data or data is corrupt - set defaults
    calData.throttleMin = 0;
    calData.throttleMax = 1024;
    calData.brakeMin = 0;
    calData.brakeMax = 1024;
    calData.clutchMin = 0;
    calData.clutchMax = 1024;
    Serial.println("No Calibration data - default has been set");
  }  
  menu();
}

void setup() {
  // G29 shifter analog inputs configuration 
  pinMode(A0, INPUT_PULLUP);   // Shifter X axis
  pinMode(A2, INPUT_PULLUP);   // Shifter Y axis

  // G29 shifter reverse switch (determins if 6th gear is 6th or reverse)
  pinMode(DI_REVERSE, INPUT); 

  // SKRS switches for Range and Split select (support for 12, 14 & 18 speed truck gearboxes)
  pinMode(DI_RANGE, INPUT_PULLUP);
  pinMode(DI_SPLIT, INPUT_PULLUP);

  // Initialize Joystick Library
  Joystick.begin(false);

  // Serial Interface for calibration menu and feedback
  Serial.begin(9600);
  while (!Serial) {
    ; // wait for serial port to connect. Needed for native USB port only
  } 

  // Set values for smoothing out the analog readings (G29 pedals have poor, noisy potentiometers)
  INA.setNoiseThreshold(10);
  INA.setSmoothWeight(4);
  INB.setNoiseThreshold(10);
  INB.setSmoothWeight(4);
  INC.setNoiseThreshold(10);
  INC.setSmoothWeight(4);

  // Load calibrations
  loadCals();
}

void loop() 
{

  // Check serial input to see if we have been told to do calibration work
  if (Serial.available() > 0)
  {
    // Something was entered on the serial port
    switch (Serial.read())
    {
      case '1':
        // Set a new calibration
        calibrate();
        break;
      case '2':
        // Erase existing calibration - set default
        calData.crcCheck = random(6145,10000);  //anything >6144 (1024*6) will cause the crc check to fail and default the vaules
        EEPROM.put(eeAddress, calData);
        Serial.println("*** Erased current calibrations ***");
        loadCals();
        break;
      case '3':
        //enable or disable data plotter output (1=Enabled, 0=Disabled)
        if (plotData == 1)
        {
          plotData = 0;
          menu();
        }
        else
        {
          plotData = 1;
        }
        break;
      default:
        // Anything arriving on the serial port that is not a menu command
        break;      
    }
  }

  // Process buttons
  for (int i=0; i<(sizeof(joystickButtons) / sizeof(joystickButtons[0])); i++)
  {
    int currentButtonState = !digitalRead(joystickButtons[i]);
    if (currentButtonState != lastButtonState[i])
    {
      Joystick.setButton(i+7, currentButtonState);  // setButton shifted by 7 (6 gears plus reverse)
      lastButtonState[i] = currentButtonState;
    }
  }

  // Translate shifter XYAxis to gears
  int x = analogRead(0);                    // Shifter X axis
  int y = analogRead(2);                    // Shifter Y axis
  currentGear=0;                            // Neutral
  if(x < HS_XAXIS_12)                       // Shifter is on the left
  {
    if(y > HS_YAXIS_135) 
    {
      // 1st gear
      currentGear=1;
    }
    if(y  <HS_YAXIS_246) 
    {
      // 2nd gear
      currentGear=2;
    }
  }
  else if(x > HS_XAXIS_56)                  // Shifter is on the right
  {
    if(y > HS_YAXIS_135) 
    {
      // 5th gear
      currentGear=5;
    }
    if(y < HS_YAXIS_246) 
    {
      // 6th gear   
      // Check reverse switch (inside G29 shifter) as gear must be 6 for it work
      if (digitalRead(DI_REVERSE) == HIGH)
      {
        // Shifter is in 6th position and switch is active, therefore reverse is selected
        currentGear=7;                      
      }
      else
      {
        // Shifter is in 6th position but switch is not active, therefore 6th gear
        currentGear=6;                      
      }
    }
  }
  else                                      // Shifter must be in the middle
  {
    if(y > HS_YAXIS_135) 
    {
      // 3rd gear
      currentGear=3;
    }
    if(y < HS_YAXIS_246) 
    {
      // 4th gear
      currentGear=4;
    }
  }
  if (currentGear != lastGear)
  {
    Joystick.setButton(lastGear-1, LOW);
    Joystick.setButton(currentGear-1, HIGH);
    lastGear = currentGear;
  }

  //Get pedal data
  int throttlePedal = INA.readSmoothed();                // Throttle pedal position (smoothed)
  int brakePedal = INB.readSmoothed();                   // Brake pedal position (smoothed)
  int clutchPedal = INC.readSmoothed();                  // Clutch pedal position (smoothed)

  //Holding variables for calculations
  int rudderUpper = 0;
  int rudderLower = 0;
  int throttleAxis = 0;
  int brakeAxis = 0;
  int clutchAxis = 0;
  int rudderAxis = 0;
  int wheelBrakeAxis = 0;

  //Apply deadzones (because of inaccurate potentiometers in the pedals)
  throttlePedal = throttlePedal - throttleDeadzone;
  brakePedal = brakePedal - brakeDeadzone;
  clutchPedal = clutchPedal - clutchDeadzone;
  if (throttlePedal < 0) throttlePedal = calData.throttleMin;
  if (brakePedal < 0) brakePedal = calData.brakeMin;
  if (clutchPedal < 0) clutchPedal = calData.clutchMin;

  //Map pedal motion to Axis (provide full 1024 range regardless of calibration range)
  throttleAxis = map(throttlePedal,calData.throttleMin,calData.throttleMax,0,1024);
  brakeAxis = map(brakePedal,calData.brakeMin,calData.brakeMax,0,1024);
  clutchAxis = map(clutchPedal,calData.clutchMin,calData.clutchMax,0,1024);

  //Removing pedal bounce that results in a negative value
  if (throttleAxis < 0) throttleAxis = 0;
  if (brakeAxis < 0) brakeAxis = 0;
  if (clutchAxis < 0) clutchAxis = 0;
  
  //Calcaulte rudder axis from Throttle and Clutch pedals (apply difference btween the two with neutral center)
  rudderUpper = map(clutchAxis,0,1024,0,512);
  rudderLower = map(throttleAxis,0,1204,0,512);
  rudderAxis = 512+(rudderUpper-rudderLower);
  Joystick.setRudder(rudderAxis);

  //Calculate wheel brake axis from the cross over of Throttle and Clutch pedals (if both pedals are pressed apply common value to the brakes)
  if (rudderUpper < rudderLower)
  {
    wheelBrakeAxis = rudderUpper;
  }
  else
  {
    wheelBrakeAxis = rudderLower;
  }
  wheelBrakeAxis = map(wheelBrakeAxis,0,512,0,1204);
  Joystick.setThrottle(wheelBrakeAxis);
  
  //Set calibrated pedal axis data
  Joystick.setXAxis(throttleAxis);
  Joystick.setYAxis(brakeAxis);
  Joystick.setZAxis(clutchAxis);

  if (plotData == 1)
  {
    Serial.print(currentGear);
    Serial.print(",");
    Serial.print(throttlePedal);
    Serial.print(",");
    Serial.print(throttleAxis);
    Serial.print(",");
    Serial.print(brakePedal);
    Serial.print(",");
    Serial.print(brakeAxis);
    Serial.print(",");
    Serial.print(clutchPedal);
    Serial.print(",");
    Serial.print(clutchAxis);
    Serial.print(",");
    Serial.print(rudderAxis);
    Serial.print(",");
    Serial.println(wheelBrakeAxis);
  }

  // Update computer with new vaules
  Joystick.sendState();
  
  // Prevent sending data too fast
  delay(10);
}
